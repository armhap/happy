import xarray as xr
import numpy as np
import itertools
import math

def open_datavar(fname, varname):
    datafilter = {
                        'filter_by_keys': {
                            'typeOfLevel': 'isobaricInhPa',
                            'shortName': varname
                        }
                     }
    d = xr.open_dataset(fname, engine='cfgrib', backend_kwargs=datafilter)
    #d.data = d.data_vars[varname]
    return d

class Region:
    def __init__(self, min_lat, min_lon, max_lat, max_lon):
        self.min_lat = min_lat
        self.min_lon = min_lon
        self.max_lat = max_lat
        self.max_lon = max_lon

def get_lat_range(lats, min_lat, max_lat):
    lat_start = int((lats > max_lat).sum())
    lat_end   = int(len(lats) - (lats < min_lat).sum())
    return slice(lat_start, lat_end)

def get_lon_range(lons, min_lon, max_lon):
    lon_start = int((lons < min_lon).sum())
    lon_end   = int(len(lons) - (lons > max_lon).sum())
    return slice(lon_start, lon_end)

class WindDataAtLocation:
    def __init__(self, lat, lon, h, u, v):
        assert list(u.isobaricInhPa) == list(v.isobaricInhPa)
        assert len(set(v.isobaricInhPa.values).difference(h.isobaricInhPa.values)) == 0
        self.lat = lat
        self.lon = lon
        self.h = h
        self.u = u
        self.v = v

    def getSpeedDataInAltitudeRange(self, min_alt, max_alt):
        result = []
        for p, u, v in zip(self.u.isobaricInhPa, self.u.values, self.v.values):
            alt = self._pressure2Altitude(p)
            if min_alt <= alt <= max_alt:
                result.append((alt, u, v))
        return result

    def _pressure2Altitude(self, p):
        return float(self.h.values[np.where(self.h.isobaricInhPa == p)[0]])


class WindDataInARegion:

    def __init__(self, fname, region):
        h = open_datavar(fname, 'gh') # Geopotential Height
        u = open_datavar(fname, 'u')  # Eastward component of the wind
        v = open_datavar(fname, 'v')  # Northward component of the wind

        # assert h.dims == u.dims
        assert u.dims == v.dims
        assert list(u.latitude) == list(v.latitude)
        assert list(u.longitude) == list(v.longitude)
        assert list(u.isobaricInhPa) == list(v.isobaricInhPa)

        lat_slice = get_lat_range(u.latitude, region.min_lat, region.max_lat)
        lon_slice = get_lon_range(u.longitude, region.min_lon, region.max_lon)

        self.u = u.data_vars['u'][:,lat_slice,lon_slice]
        self.v = v.data_vars['v'][:,lat_slice,lon_slice]
        self.h = h.data_vars['gh'][:,lat_slice,lon_slice]

    @property
    def lats(self):
        return self.u.latitude

    @property
    def lons(self):
        return self.u.longitude

    def dataAt(self, lat, lon):
        latIndex = int(np.where(self.lats == lat)[0])
        lonIndex = int(np.where(self.lons == lon)[0])
        h = self.h[:,latIndex,lonIndex]
        u = self.u[:,latIndex,lonIndex]
        v = self.v[:,latIndex,lonIndex]
        return WindDataAtLocation(lat, lon, h, u, v)

def angle(a, b):
    "Returns the signed angle between 2d vectors a and b"

    c = float(np.dot(a, b))/(np.linalg.norm(a)*np.linalg.norm(b))
    angle = math.acos(c)
    return angle if np.cross(a, b) > 0 else -angle

def nonNegativeSolutionExists(v1, v2, v3):
    """Does the equation t1*v1 + t2*v2 + t3*v3 = 0 have a non-negative solution?

    Given 3 2d vectors, this function tells whether the equation

        t1*v1 + t2*v2 + t3*v3 = 0

    has a non-trivial non-negative solution (none of the t's is negative, at
    least one of them is non-zero)."""

    a12 = angle(v1, v2)
    a23 = angle(v2, v3)
    solutionExists = abs(a12 + a23) > math.pi
    if solutionExists:
        print(v1, v2, v3)
    return solutionExists

class Balloon:
    def __init__(self):
        pass

    def windProfileAllowsLoitering(self, windProfile):
        """Checks if the wind profile allows loitering over the same location

        The current implementation assumes that the balloon can switch altitude
        instantly and has no intertia (i.e. always drifts in the direction and
        at the speed of the wind at its current altitude). The loiterability
        criterion then boils down to checking if the wind profile contains three
        vectors V1, V2, and V3 such that the equation

            t1*V1 + t2*V2 + t3*V3 = 0

        has a non-trivial solution with **non-negative** t's. Such a solution
        corresponds to the balloon spending times t1, t2, and t3 at the 1st,
        2nd and 3rd levels respectively (and returning the the start location
        at the end of the t1+t2+t3 cycle).
        """

        for c in itertools.combinations(windProfile, 3):
            if nonNegativeSolutionExists(*(x[1:] for x in c)):
                return True
        return False

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--min-lat', type=float, required=False, default=38)
    parser.add_argument('--max-lat', type=float, required=False, default=42)
    parser.add_argument('--min-lon', type=float, required=False, default=40)
    parser.add_argument('--max-lon', type=float, required=False, default=50)
    parser.add_argument('--min-alt', type=float, required=False, default=15000)
    parser.add_argument('--max-alt', type=float, required=False, default=25000)
    parser.add_argument('gfs_grib_file')
    args = parser.parse_args()

    fname = args.gfs_grib_file

    region = Region(args.min_lat, args.min_lon, args.max_lat, args.max_lon)

    regionWindData = WindDataInARegion(fname, region)

    b = Balloon()

    for lat in regionWindData.lats:
        for lon in regionWindData.lons:
            wd = regionWindData.dataAt(float(lat), float(lon))
            print(wd.lat, wd.lon, end=': ')
            sd = wd.getSpeedDataInAltitudeRange(args.min_alt, args.max_alt)
            if b.windProfileAllowsLoitering(sd):
                print('OK')
            else:
                print('-')
