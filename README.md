# Quick start

1. Set up the conda environment

```
./create_conda_env
```

2. ... and activate it

```
conda activate hap
```

3. Download GFS-ANL data from https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/global-forcast-system-gfs

Example:

```
wget https://www.ncei.noaa.gov/data/global-forecast-system/access/historical/analysis/202005/20200501/gfsanl_3_20200501_0000_000.grb2
```

4. Run the `untethered_hap.py` script

```
python untethered_hap.py --min-lat 40 --max-lat 41 --min-lon 40 --max-lon 41 gfsanl_3_20200501_0000_000.grb2
```
